import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import * as Web3 from 'web3';
import {ARM} from './ARM';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  btnLabel = 'Click me';
  private web3Inst: Web3;
  private armAddress = '0x581634b05df3ff20cd5fc463d23455d5bba2c2f3';
  private armContract: ARM;

  ngOnInit(): void {
    const wa = window as any;
    if (window.hasOwnProperty('web3')) {
      this.web3Inst = new Web3(wa.web3.currentProvider);
    } else {
      this.web3Inst = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }
    this.armContract = new ARM(this.web3Inst, this.armAddress);
  }

  public btnClicked = (f: NgForm): void => {
    this.sendTransaction(f.value.value);
  };

  private sendTransaction = (value: number): void => {
    const accounts: string[] = this.web3Inst.eth.accounts;
    this.armContract.transferTx('0x70E644d00EcdF0BDBD0e8Fe94d9c7f10F15216FD', this.web3Inst.toWei(value, 'ether'))
      .send({from: accounts[0]});
  }
}
